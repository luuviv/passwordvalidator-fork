package password;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * @author Vivian Luu, 991558427
 * 
 * 
 * This class will be created and developed using TDD
 * 
 * */
public class PasswordValidatorTest {

	//LAB 5 - Part 2
	@Test
	public void testHasValidCaseChars() {
		boolean result = PasswordValidator.hasValidCaseChars("aAaA");
		
		assertTrue("Invalid case chars", result);
	}
	
	@Test//Exception Blank 
	public void testHasValidCaseCharsExceptionBlank() {
		boolean result = PasswordValidator.hasValidCaseChars("");
		assertFalse("Invalid case chars", result);
	}

	//Exception  Null
	@Test
	public void testHasValidCaseCharsExceptionNull() {
		boolean result = PasswordValidator.hasValidCaseChars(null);
		assertFalse("Invalid case chars", result);
	}
	
	//Exception  Null
	@Test
	public void testHasValidCaseCharsExceptionNumbers() {
		boolean result = PasswordValidator.hasValidCaseChars("898989");
		assertFalse("Invalid case chars", result);
	}
	
	//Boundary Out - Upper
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		boolean result = PasswordValidator.hasValidCaseChars("AAAA");
		assertFalse("Invalid case chars", result);
	}
	
	//Boundary Out - Lower
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		boolean result = PasswordValidator.hasValidCaseChars("aaaa");
		assertFalse("Invalid case chars", result);
	}
	
	//Boundary In - one each 
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		boolean result = PasswordValidator.hasValidCaseChars("Aa");
		assertTrue("Invalid case chars", result);
	}
	
	
	
//	@Test
//	public void testIsValidLength() {
//		
//		fail("invalid password length");
//	}
	
	//Regular
	@Test 
	public void testIsValidLength() {
		
		assertTrue("invalid password length", PasswordValidator.isValidLength("123456789"));
	}
	
	//Exception
	@Test 
	public void testIsValidLengthException() {
		
		assertFalse("invalid password length", PasswordValidator.isValidLength(null));
	}
	
	//Boundary In
	@Test 
	public void testIsValidLengthBoundaryIn() {
		
		assertTrue("invalid password length", PasswordValidator.isValidLength("12345678"));
	}
	
	//Boundary Out
	@Test 
	public void testIsValidLengthBoundaryOut() {
		
		assertFalse("invalid password length", PasswordValidator.isValidLength("1234567"));
	}
	
	//Regular
	@Test
	public void testHasEnoughDigits() {
		
		assertTrue("invalid password length, not enough digits", PasswordValidator.hasEnoughDigits("12"));
	}
	
	//Exception
	@Test
	public void testHasEnoughDigitsException() {
		
		assertFalse("invalid password length, not enough digits", PasswordValidator.hasEnoughDigits("  "));
	}
	
	//BoundaryIn
	@Test
	public void testHasEnoughDigitsBoundaryIn() {
		
		assertTrue("invalid password length, not enough digits", PasswordValidator.hasEnoughDigits("12 "));
	}
	
	//Boundary Out
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		
		assertFalse("invalid password length, not enough digits", PasswordValidator.hasEnoughDigits("1"));
	}
	
}